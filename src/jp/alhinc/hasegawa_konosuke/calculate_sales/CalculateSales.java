package jp.alhinc.hasegawa_konosuke.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class CalculateSales {
	public static void main(String[] args) {
		if(args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		//Hashmapを新しく作った
		HashMap<String, String>map = new HashMap<String, String>();

		//売り上げ金額を集計させていくマップ(売上金額の初期値"0")
		HashMap<String, Long>sumMap = new HashMap<String, Long>();

		//支店定義ファイル（branch.lst）を読み込む
		if(! readFile(args[0], "branch.lst", map, sumMap,"支店", "[0-9]{3}")) {
			return;
		}

		//コマンドライン引数で設定したディレクトリ内のフォルダーを読み込む
		File dirl = new File(args[0]);
		File[] list = dirl.listFiles();

		//0000001.rcdのように8桁連番の.rcdファイルを格納するリストを作った
		ArrayList<File> salesList = new ArrayList<File>();
		for(int i = 0  ; i < list.length; i++ ) {
			//８桁連番.rcd かつ ファイルであるものをリストにaddしている
			if(list[i].getName().matches("[0-9]{8}.rcd$") && list[i].isFile()) {
				salesList.add(list[i]);
			}
		}
		//ファイル名の連番のエラーやる
		for(int i = 0; i < salesList.size() - 1; i++ ) {
			//saleslist<file>からfileを持ってきて、getNameして数字部分だけ抽出している
			int fileName = Integer.parseInt( (salesList.get(i).getName().substring(0, 8)));
			int nextFileName = Integer.parseInt( (salesList.get(i+1).getName().substring(0, 8)));
			int nameCheack = (nextFileName - fileName);
			if(nameCheack != 1){
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}
		//売り上げファイルを読み込む
		BufferedReader br2 = null;
		
		for(int i = 0; i < salesList.size(); i++ ) {
			try {
				//売り上げファイルのデータを保持させるリストを作る
				ArrayList <String> codeList = new ArrayList<String>();
				FileReader fr2 = new FileReader(salesList.get(i));
				br2 = new BufferedReader(fr2);
				String line;
				while((line = br2.readLine()) != null) {
					//codeListにデータを追加
					codeList.add(line);
				}

				/*売り上げファイルのコードを格納しているリストは２行だけなのでそれ以外で
				 エラー表示*/
				if(codeList.size() != 2) {
					System.out.println(salesList.get(i) .getName() + "のフォーマットが不正です");
					return;
				}
				//支店に該当がない場合のエラー処理
				if(map.get(codeList.get(0)) == null) {
					//支店コードが一致しないのでエラー表示
					System.out.println(salesList.get(i).getName() + "の支店コードが不正です");
					return;
				}
				//売上金額が数字で正規表現をかけてエラー処理
				if(codeList.get(1).matches("^[0-9]*$") == false) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}

				//String型からLong型に変換
				long ln = Long.parseLong(codeList.get(1) );

				/*sumMap（売上金額を格納するマップ）に値を格納+計算させる
				/初期値と売上金額を集計*/
				Long salesamount = sumMap.get(codeList.get(0)) + ln;

				//集計した値をsumMapに格納した
				sumMap.put(codeList.get(0),salesamount);

				//sumMapに格納したのちにfor文が回っているので、コードが被っていても集計できる

				//合計金額が10桁を超えた場合のエラー処理
				if(salesamount > 9999999999L) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}
			}catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました");
			}finally {
				if(br2 != null) {
					try {
						br2.close();
					}catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました");
					}
				}
			}
		}
		/*outPutFileメソッド(ファイルに出力するメソッド)
		参照するディレクトリ名,参照するファイル名,参照するHashMap名(branch.latが格納されているマップ名),参照するHashMap（.rcdが格納されているマップ名）*/
		//branch.outにデータを出力する
		if(! outputFile(args[0],"branch.out",map, sumMap)) {
			return;
		}
	}
	//branch.lstを読み込むメソッド
	public static boolean readFile(String directory, String fileName, HashMap<String, String>branchListMap, HashMap<String, Long>rcdMap,String fileType, String shopCodeExpression) {
		BufferedReader br = null;
		try {
			File file = new File(directory,fileName);

			//支店定義ファイルの存在判定
			//支店定義ファイルが無い場合のエラー文表示
			if(!file. exists()) {
				System.out.println(fileType + "定義ファイルが存在しません");
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String line;
			while((line = br.readLine()) != null){
				//売り上げデータを(",")で区切った
				String[] code = line.split(",");

				if(code.length != 2) {
					System.out.println(fileType + "定義ファイルのフォーマットが不正です");
					return false;
				}

				//支店定義ファイルのフォーマットを確認する
				if(!code[0].matches(shopCodeExpression) ) {
					System.out.println(fileType + "定義ファイルのフォーマットが不正です");
					return false;
				}
				//mapとHashmapに支店コードと支店名を入れている
				branchListMap.put(code[0],code[1]);
				rcdMap.put(code[0], 0L);
		 	}
		}catch(IOException e) {
			 System.out.println("予期せぬエラーが発生しました");
			 return false;
		}finally {
			 if(br != null) {
				 try {
					 br.close();
				 }catch(IOException e){
					 System.out.println("予期せぬエラーが発生しました");
					 return false;
				 }
			 }
		}return true;
	}
	public static boolean outputFile(String directory, String fileName, HashMap<String, String> branchLstMap, HashMap<String, Long> rcdMap) {

		BufferedWriter bw = null;

		try{
			File file = new File(directory, fileName);
			FileWriter fw = new FileWriter (file);
			bw = new BufferedWriter(fw);

			//mapとsumMapのkeyとvalueを拡張for文で取り出す
			for (HashMap.Entry<String, String> entry : branchLstMap.entrySet()) {
				//System.out.println(entry.getKey() + "," + entry.getValue() + "," + sumMap.get(entry.getKey()));
				bw.write(entry.getKey() + "," + entry.getValue() + "," + rcdMap.get(entry.getKey()) );
				bw.newLine();
			}
		}catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally {
			if(bw != null) {
				try {
					bw.close();
				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}return true;
	}
}